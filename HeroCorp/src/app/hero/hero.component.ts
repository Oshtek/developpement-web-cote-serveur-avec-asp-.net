import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  @Input() heroCourant:Hero;
  @Output() newHero=new EventEmitter<Hero>();
  constructor() { }

  ngOnInit(): void {
    if(this.heroCourant==null){
      this.heroCourant=new Hero();

    }


  }

  public createHero(){
    console.log("new hero from hero component");
    this.newHero.emit(this.heroCourant);
  }

}
