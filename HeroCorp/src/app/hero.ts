export class Hero {

    private _id: number;

    private _nom: string;

    private _puissance: number;

    private _description: string;


  public get description(): string {
    return this._description;
  }
  public set description(value: string) {
    this._description = value;
  }


constructor(){
  this.id=0;
}
  public get puissance(): number {
    return this._puissance;
  }
  public set puissance(value: number) {
    this._puissance = value;
  }


  public get nom(): string {
    return this._nom;
  }
  public set nom(value: string) {
    this._nom = value;
  }
    public get id(): number {
      return this._id;
    }
    public set id(value: number) {
      this._id = value;
    }
}
