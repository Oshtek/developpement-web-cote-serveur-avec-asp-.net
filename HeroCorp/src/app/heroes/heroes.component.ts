import { Hero } from './../hero';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes:Array<Hero>;
  heroTemp:Hero;
  constructor() { }

  ngOnInit(): void {
    this.heroes=new Array<Hero>();
    this.heroTemp=new Hero();

  }

  public addHero(hero:Hero){
    hero.id=this.heroes.length+1;
    this.heroes.push(hero);
    console.log(this.heroes);
    this.heroTemp=new Hero();
  }

}
