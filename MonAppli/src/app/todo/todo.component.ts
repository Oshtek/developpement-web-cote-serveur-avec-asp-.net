import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Todo } from '../todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  @Input() tache:Todo;



  @Output() sortie=new EventEmitter<Todo>();

  public exit(){
    console.log("on va quitter "+this.tache)
    this.sortie.emit(this.tache);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
