import { Component, OnInit } from '@angular/core';
import { Todo } from '../todo';


@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {

  tache1:Todo;
  todos: Array<Todo>;
  constructor() { }

  ngOnInit(): void {
    this.tache1=new Todo();
    this.tache1.id=0;
    this.tache1.nom="Pas de tache pour l'instant";
    this.todos=new Array<Todo>();
  }

  public CreerTache(){
    this.todos.push(this.tache1);
    this.tache1=new Todo();
  }
 public  exitFromTodo(tache:Todo){
  console.log("On a quitté la tache "+tache);
 }
}
