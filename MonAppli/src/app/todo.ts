export class Todo {
  private _id: number;

  private _nom: string;

  private _commentaire: string;

  private _etat: boolean;



  public get etat(): boolean {
    return this._etat;
  }
  public set etat(value: boolean) {
    this._etat = value;
  }

  public get commentaire(): string {
    return this._commentaire;
  }
  public set commentaire(value: string) {
    this._commentaire = value;
  }

  public get nom(): string {
    return this._nom;
  }
  public set nom(value: string) {
    this._nom = value;
  }
  public get id(): number {
    return this._id;
  }
  public set id(value: number) {
    this._id = value;
  }

  public toString(){
    return this.nom;
  }

}
