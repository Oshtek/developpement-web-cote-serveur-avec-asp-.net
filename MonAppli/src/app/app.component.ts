import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   titre: string = 'Coucou';
   nombreDeClient:number=10;
   status:boolean=true;
  constructor(){
    this.nombreDeClient=5;

  }

  changeMe(){
    this.status=!this.status;
  }
}
